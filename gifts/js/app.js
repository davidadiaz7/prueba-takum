window.scope = null;
var app = angular.module("giftsApp", ['gettext', 'ngRoute', 'angularModalService']);

app.controller('langCtrl', ['$scope', 'gettextCatalog', function($scope, gettextCatalog)
{
	$scope.langs = ["es_CO","en_US"];
	$scope.lang = "es_CO";

	$scope.updateLang = function()
	{
		gettextCatalog.setCurrentLanguage($scope.lang);
	}
}]);

app.run(['gettextCatalog', function (gettextCatalog) 
{
	gettextCatalog.currentLanguage = "es_CO";
}]);


app.controller('GiftsController', ['$scope', '$http', '$location', function($scope, $http, $location){
	this.login = function(){
		var encodedString =
			"username=" + encodeURIComponent($scope.inputUsername) +
			"&password=" + encodeURIComponent($scope.inputPassword);
		$http({
			method: 'POST',
			url: 'backend/authentication.php',
			data: encodedString,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function onSuccess(response) {
			var headers = response.headers;
			var data = response.data;
			console.log(headers);
			console.log(data);
			if(data.data.length > 0){
				var user = data.data[0];
				localStorage.setItem('UserLogin', JSON.stringify(user));
				$location.url('/productos');
			} else {
				alert('Datos incorrectos');
			}
		});
	};

	this.logout = function(){
		localStorage.clear();
		$location.url('/');
	};

	this.checkLogin = function() {
		var UserLogin = localStorage.getItem('UserLogin');
		if(UserLogin != undefined){
			$location.url('/productos');
		}
	}

	this.checkLogin();
}]);

app.controller('ProductsController', ['$scope', '$http', 'ModalService', 'gettextCatalog', function($scope, $http, ModalService, gettextCatalog){
	$scope.listCategories = [];
	$scope.listProducts = [];
	$scope.activeCategory = 0;
	$scope.showModal = false;
	$scope.idProduct = 0;
	$scope.modalInstance = null;
	this.getCategories = function(){
		var UserLogin = JSON.parse(localStorage.getItem('UserLogin'));
		$('.span-name').html(UserLogin.name + ', ' + UserLogin.rolName);
		$http({
			method: 'GET',
			url: 'backend/categories.php',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function onSuccess(response) {
			var headers = response.headers;
			var data = response.data;
			console.log(headers);
			console.log(data);
			if(data.data.length > 0){
				$scope.listCategories = data.data;
			}
		});
	};

	this.onclickCategory = function(categoryId){
		$scope.activeCategory = categoryId;
		sessionStorage.setItem('activeCategory', categoryId);
		var UserLogin = JSON.parse(localStorage.getItem('UserLogin'));
		var encodedString =
			"categoryId=" + categoryId +
			"&rolId=" + UserLogin.rol_id + "&type=1";
		$http({
			method: 'POST',
			url: 'backend/products.php',
			data: encodedString,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function onSuccess(response) {
			var headers = response.headers;
			var data = response.data;
			console.log(headers);
			console.log(data);
			$scope.listProducts = [];
			$scope.listProducts.length = 0;
			if(data.data.length > 0){
				$scope.listProducts = data.data;
			}
			sessionStorage.setItem('parentScope', JSON.stringify($scope.listProducts));
			console.log('a', $scope);
			window.scope = $scope;
		});
	}

	this.onclickCreate = function(){
			console.log('b', $scope);
        ModalService.showModal({
            templateUrl: 'pages/modal.html',
            controller: "ProductsController"
        }).then(function(modal) {
            modal.element.modal();
        });
	}

	this.saveProduct = function(){
			console.log('c', $scope);
			console.log('d', window.scope);
		var idProduct = $scope.inputId;
		var UserLogin = JSON.parse(localStorage.getItem('UserLogin'));
		if(idProduct == 0 || idProduct == undefined){
			var encodedString =
				"name=" + encodeURIComponent($scope.inputName) +
				"&description=" + encodeURIComponent($scope.inputDesc)+
				"&cost=" + encodeURIComponent($scope.inputCost)+
				"&category_id=" + encodeURIComponent($scope.inputCategory)+
				"&rol_id=" + encodeURIComponent(UserLogin.rol_id)+
				"&type=2";
			$http({
				method: 'POST',
				url: 'backend/products.php',
				data: encodedString,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function onSuccess(response) {
				var headers = response.headers;
				var data = response.data;
				console.log(headers);
				console.log(data);
				if(data.status === 'OK'){
					alert('Almacenado correctamente');
					window.scope.productsCtrl.onclickCategory(sessionStorage.getItem('activeCategory'));
					$('.myModal').modal('hide');
				} else {
					alert('Error almacenando');
				}
			});
		}else {
			var encodedString =
				"name=" + encodeURIComponent($scope.inputName) +
				"&description=" + encodeURIComponent($scope.inputDesc)+
				"&cost=" + encodeURIComponent($scope.inputCost)+
				"&category_id=" + encodeURIComponent($scope.inputCategory)+
				"&rol_id=" + encodeURIComponent(UserLogin.rol_id)+
				"&id=" + encodeURIComponent(idProduct)+
				"&type=3";
			$http({
				method: 'POST',
				url: 'backend/products.php',
				data: encodedString,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function onSuccess(response) {
				var headers = response.headers;
				var data = response.data;
				console.log(headers);
				console.log(data);
				if(data.status === 'OK'){
					alert('Editado correctamente');
					window.scope.productsCtrl.onclickCategory(sessionStorage.getItem('activeCategory'));
					$('.myModal').modal('hide');
				} else {
					alert('Error editando');
				}
			});
		}
	};

	this.onClickEdit = function(productId){
		$scope.idProduct = productId;
		var productSelected = null;
		for(var i = 0, len = $scope.listProducts.length; i < len; i++){
			if(parseInt($scope.listProducts[i].id) === parseInt(productId)){
				productSelected = $scope.listProducts[i];
			}
		}

		ModalService.showModal({
            templateUrl: 'pages/modal.html',
            controller: "ProductsController"
        }).then(function(modal) {
            modal.element.modal();
            modal.scope.inputName = productSelected.name;
			modal.scope.inputDesc = productSelected.description;
			modal.scope.inputCost = parseInt(productSelected.cost);
			modal.scope.inputId = productId;
			modal.scope.activeCategory = $scope.activeCategory;
			modal.scope.inputCategory = productSelected.category_id;
        });
		
	};

	this.onClickDelete = function(productId){
		var encodedString =
			"id=" + productId + "&type=4";
		$http({
			method: 'POST',
			url: 'backend/products.php',
			data: encodedString,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}).then(function onSuccess(response) {
			var headers = response.headers;
			var data = response.data;
			console.log(headers);
			console.log(data);
			if(data.status === 'OK'){
				alert('Eliminado correctamente');
				window.scope.productsCtrl.onclickCategory(sessionStorage.getItem('activeCategory'));
			} else {
				alert('Error elimiando');
			}
		});
	}

	this.getCategories();
}]);

app.config(['$routeProvider', '$locationProvider',function($routeProvider, $locationProvider) {
  
  	$routeProvider.when('/', {
		templateUrl: "pages/login.html",
		controller: "GiftsController"
	}); 

	$routeProvider.when('/productos', {
		templateUrl: "pages/products.html",
		controller: "GiftsController"
	}); 

	$locationProvider.html5Mode(true);
}]);