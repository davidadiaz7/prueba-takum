<?php
	header("Access-Control-Allow-Origin: *");
  	header("Content-Type: application/json; charset=UTF-8");

	require_once 'db.php';

	global $conn;

	$typeRequest = $_POST [ 'type' ];
	$outp = "";

	if($typeRequest == 1){
		 
		$result = $conn->query("SELECT id, name, description, cost from products WHERE
			category_id='" . $_POST [ 'categoryId' ] . "' && rol_id='" .    $_POST [ 'rolId' ] . "'" );
		  
		while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
		    if ($outp != "") {$outp .= ",";}
		    $outp .= '{"id":"'  . $rs["id"] . '",';
		    $outp .= '"name":"'   . $rs["name"]        . '",';
		    $outp .= '"description":"'   . $rs["description"]        . '",';
		    $outp .= '"category_id":"'   . $_POST [ 'categoryId' ]        . '",';
		    $outp .= '"cost":"'. $rs["cost"]     . '"}';
		}
		$outp ='{"data":['.$outp.']}';
	}else if($typeRequest == 2){
		 
		$result = $conn->query("INSERT INTO products (name, description, cost, category_id, rol_id) 
			VALUES ('" . $_POST [ 'name' ] . "', '" . $_POST [ 'description' ] . "', '" . $_POST [ 'cost' ] . "'," . $_POST [ 'category_id' ] . " ," . $_POST [ 'rol_id' ] . ")" );
		 
		 
		if($result === TRUE) {
			$outp ='{"status":"OK"}';
		}else{
			$outp ='{"status":"FAILURE"}';
		}
	} else if($typeRequest == 3){
		 
		$result = $conn->query("UPDATE products set name = '" . $_POST [ 'name' ] . "', description = '" . $_POST [ 'description' ] . "', cost = '" . $_POST [ 'cost' ] . "', category_id = " . $_POST [ 'category_id' ] . " , rol_id = " . $_POST [ 'rol_id' ] . " WHERE id = " . $_POST [ 'id' ] . "" );
		 
		 
		if($result === TRUE) {
			$outp ='{"status":"OK"}';
		}else{
			$outp ='{"status":"FAILURE"}';
		}
	} else if($typeRequest == 4){
		 
		$result = $conn->query("DELETE from products WHERE id = " . $_POST [ 'id' ] . "" );
		 
		 
		if($result === TRUE) {
			$outp ='{"status":"OK"}';
		}else{
			$outp ='{"status":"FAILURE"}';
		}
	}

	$conn->close();
	 
	echo($outp);
?>