<?php
	header("Access-Control-Allow-Origin: *");
  	header("Content-Type: application/json; charset=UTF-8");

	require_once 'db.php';

	global $conn;
	 
	$result = $conn->query("SELECT id, name from categories" );
	 
	$outp = "";
	  
	while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
	    if ($outp != "") {$outp .= ",";}
	    $outp .= '{"id":"'  . $rs["id"] . '",';
	    $outp .= '"name":"'   . $rs["name"]        . '"}';
	}
	$outp ='{"data":['.$outp.']}';
	$conn->close();
	 
	echo($outp);
?>