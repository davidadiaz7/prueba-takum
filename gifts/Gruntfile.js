module.exports = function(grunt) {
 
  // Project configuration.
  grunt.initConfig({
   nggettext_extract: {
     pot: {
       files: {
         'po/template.pot': ['**/*.html']
       }
     },
   }, 
   nggettext_compile: {
     all: {
       files: {
         'translations.js': ['po/*.po']
       }
     },
   },
 })
 
  grunt.loadNpmTasks('grunt-angular-gettext');
 
};